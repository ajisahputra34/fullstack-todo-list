<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $post = Post::paginate(5);

        return response()->json([
            'message' => 'Berhasil mengambil semua data post.',
            'data' => $post,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'desc' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => ['Gagal membuat data post baru.', $validator->errors()],
            ], 422);
        }

        $post = new Post([
            'title' => $request->title,
            'desc' => $request->desc,
        ]);

        $post->save();

        return response()->json([
            'message' => 'Berhasil membuat data post baru.',
            'data' => $post,
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        try {
            $ID = Crypt::decryptString($id);
            $data = Post::find($ID);

            return response()->json([
                'message' => 'Berhasil mengambil data post.',
                'data' => $data,
            ]);
        } catch (\Illuminate\Contracts\Encryption\DecryptException $e) {
            return response()->json([
                'message' => 'Gagal mengambil data user.',
                'data' => null,
            ], 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        try {
            $ID = Crypt::decryptString($id);
            $validator = Validator::make($request->all(), [
                'title' => 'required',
                'desc' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'message' => ['Gagal update data.', $validator->errors()],
                ], 422);
            }

            $post = Post::find($ID);
            $post->update([
                'title' => $request->title,
                'desc' => $request->desc,
            ]);

            return response()->json([
                'message' => 'Berhasil update data post.',
                'data' => $post,
            ]);

        } catch (\Illuminate\Contracts\Encryption\DecryptException $e) {
            return response()->json([
                'message' => 'Gagal update data post.',
                'data' => null,
            ], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $ID = Crypt::decryptString($id);
            Post::find($ID)->delete();

            return response()->json([
                'message' => 'Data berhasil dihapus.',
            ]);
        } catch (\Illuminate\Contracts\Encryption\DecryptException $e) {
            return response()->json([
                'message' => 'Gagal hapus data post.',
                'data' => null,
            ], 400);
        }
    }
}
