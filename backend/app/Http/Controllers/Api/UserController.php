<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Crypt;

class UserController extends Controller
{
    public function allUser()
    {
        $users = User::all();

        return response()->json([
            'message' => 'Berhasil mengambil semua data user.',
            'data' => $users,
        ]);
    }

    public function getUser(string $id)
    {
        try {
            $ID = Crypt::decryptString($id);

            $user = User::find($ID);

            return response()->json([
                'message' => 'Berhasil mengambil data user.',
                'data' => $user,
            ]);
        } catch (\Illuminate\Contracts\Encryption\DecryptException $e) {
            return response()->json([
                'message' => 'Gagal mengambil data user.',
                'data' => null,
            ], 400);
        }
    }
}
