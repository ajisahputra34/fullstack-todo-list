<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ApiResponseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $response = $next($request);
        if ($response instanceof \Illuminate\Http\JsonResponse ) {
            $data = $response->getData();

            // Strukturkan respon sesuai format yang diinginkan
            $formattedResponse = [
                'status' => $response->getStatusCode(),
                'message' => isset($data->message) ? $data->message : null,
                'result' => isset($data->data) ? $data->data : null,
            ];

            $response->setData($formattedResponse);
        }

        return $response;
    }
}
