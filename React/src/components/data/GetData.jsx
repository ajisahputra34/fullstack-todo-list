import React, { useState, useEffect } from "react"
import axios from 'axios'
import config from './config'

function DataComponent () {
    const [data, setData] = useState([])
    const [currentPage, setCurrentPage] = useState(1)
    const [totalPages, setTotalPages] = useState(0)

    const api = axios.create({
        baseURL: config.apiUrl,
    })

    useEffect(() => {
        api.get(`http://localhost:8000/api/post?page=${currentPage}`)
        .then(response => {
            setData(response.data.result.data)
            setTotalPages(response.data.result.last_page)
        })
        .catch(error => {
            console.log(error)
        })
    }, [currentPage])

    return (
        <>
            <div className="max-h-97 overflow-scroll mt-3 mb-5">
                <table className="table bg-white text-black w-full max-h-screen text-center">
                    <thead className="border-2 border-black">
                        <tr>
                            <th className="border-2 border-black">No</th>
                            <th className="border-2 border-black">Todo List</th>
                            <th className="border-2 border-black">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data.map((item, index) => (
                            <tr key={item.id}>
                                <th className="border-2 border-black p-2">{index + 1}</th>
                                <td className="border-2 border-black">{item.title}</td>
                                <td className="border-2 border-black p-2 w-72">
                                    <button className="btn btn-accent">Show</button>
                                    <button className="btn btn-warning mx-3">Edit</button>
                                    <button className="btn btn-error">Delete</button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
            <div className="flex justify-end">
                <div className="join">
                    <button className="join-item btn bg-white text-black hover:text-white" disabled={currentPage === 1} onClick={() => setCurrentPage(currentPage - 1)}>«</button>
                    {[...Array(totalPages)].map((_, index) =>(
                        <button key={index} className={`join-item btn bg-white text-black hover:text-white ${currentPage === index + 1 ? 'font-bold' : ''}`} onClick={() => setCurrentPage(index + 1)}>
                            {index + 1}
                        </button>
                    ))}
                    <button className="join-item btn bg-white text-black hover:text-white" disabled={currentPage === totalPages} onClick={() => setCurrentPage(currentPage + 1)}>»</button>
                </div>
            </div>
        </>
    )
}

export default DataComponent