import GetData from './data/GetData'
function Table () {
    return (
            <div className="h-screen w-full bg-white p-5 rounded-3xl box-border">
                <button className="btn btn-primary">Buat Todo List</button>

                <GetData/>
            </div>
    )
}

export default Table