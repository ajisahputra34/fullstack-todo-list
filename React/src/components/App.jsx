import reactLogo from '../assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'

function App() {
  return (
    <>
      <div className='grid justify-center'>
        <div className='m-auto flex w-1/2 gap-10 justify-end items-center'>
            <a href="https://vitejs.dev" target="_blank" rel="noreferrer">
              <img src={viteLogo} className="logo  w-96" alt="Vite logo" />
            </a>
            <a href="https://react.dev" target="_blank" rel="noreferrer">
              <img src={reactLogo} className="logo  w-96 react" alt="React logo" />
            </a>
        </div>
        <h1 className='m-auto text-3xl mt-10 font-semibold'>Vite + React</h1>
      </div>
    </>
  )
}

export default App
