import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './components/App'
import Table from './components/Table'
import './index.css'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <div className='max-h-screen column md:flex items-center'>
      <App />
      <Table />
    </div>
  </React.StrictMode>,
)
